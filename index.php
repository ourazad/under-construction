<?php
$page = "home";
include_once 'pages/header.php';
?>
<div class="container">
    <div class="row">
        <div class="col-12 my-5 py-5 text-center text-muted">
            <h2 class="py-5">This site is under construction</h2>
            <i class="fas fa-cogs fa-8x fa-spin py-5" style="animation: a 5s infinite linear;"></i>
        </div>
    </div>
</div>
<?php
include_once 'pages/footer.php';
?>