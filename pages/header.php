<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="includes/fontawesome-free-5.2.0-web/css/all.min.css">
        <link rel="stylesheet" href="includes/css/bootstrap.min.css">
        <link rel="stylesheet" href=" https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
        <link rel="stylesheet" href="includes/css/gallery-grid.css">
        <link rel="stylesheet" href="includes/css/style.css">
        <!--        title & logo-->
        <!--<link rel="icon" href="includes/images/logo.jpg" type="image/x-icon">-->
        <title>Dolceeamarobd</title>
    </head>
    <body>
        <div class="header_info ">
            <div class="container">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-4 logo m-0 p-0"> <!-- d-none d-md-block -->
<!--                            <a class="navbar-brand" href="index.php"><img  src="includes/images/logo.jpg" width="200"></a>-->
                            <a class="navbar-brand pt-3" href="index.php"><h1 class="text-capitalize">dolcee amaro</h1></a>
                        </div>
<!--                        <div class="col-12 col-md-8 info d-none d-md-block">-->
<!--                            <ul class="pt-0 pt-md-3">-->
<!--                                <li class="pt-3 pt-md-0 pl-0 pl-md-0 pl-lg-4">-->
<!--                                    <i class="fas fa-phone fa-circle icone"></i>-->
<!--                                    <span class="text pl-3">-->
<!--                                        <strong>+8801716-630458</strong>-->
<!--                                        <br>-->
<!--                                        <small>Get in touch with us</small>-->
<!--                                    </span>-->
<!--                                </li>-->
<!--                                <li class="pt-3 pt-md-0  pl-0 pl-md-1 pl-lg-4">-->
<!--                                    <i class="far fa-envelope fa-circle icone"></i>-->
<!--                                    <span class="text pl-3">-->
<!--                                        <strong>info@helvetios.co</strong>-->
<!--                                        <br>-->
<!--                                        <small>Send us an e-mail</small>-->
<!--                                    </span>-->
<!--                                </li>-->
<!--                            </ul>-->
<!--                        </div>-->
                    </div>
                </div>
            </div>
        </div>
        <!------------------------------------ End Header Info ------------------------------------------------------>
        <nav class="navbar navbar-expand-lg navbar-dark d-bg-red sticky-top">
            <div class="container">
<!--                <a class="navbar-brand d-block d-md-none" href="index.html"><img  src="includes/images/logo.jpg" width="200"></a>-->
                <a class="navbar-brand d-block d-md-block d-lg-none"><strong>MENU</strong></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon toggler-color"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0 custom_nav">
                        <li class="nav-item <?php
                        if ($page == 'home') {
                            echo 'active';
                        }
                        ?>">
                            <a class="nav-link" href="index.php">Home</a>
                        </li>
<!--                        <li class="nav-item --><?php
//                        if ($page == 'about') {
//                            echo 'active';
//                        }
//                        ?><!--">-->
<!--                            <a class="nav-link" href="#">ABOUT</a>-->
<!--                        </li>-->
<!--                        <li class="nav-item --><?php
//                        if ($page == 'management') {
//                            echo 'active';
//                        }
//                        ?><!--">-->
<!--                            <a class="nav-link" href="management.php">MANAGEMENT</a>-->
<!--                        </li>-->
<!---->
<!--                        <li class="nav-item --><?php
//                        if ($page == 'work') {
//                            echo 'active';
//                        }
//                        ?><!--">-->
<!--                            <a class="nav-link" href="work.php">WORK</a>-->
<!--                        </li>-->
<!---->
<!--                        <li class="nav-item --><?php
//                        if ($page == 'gents') {
//                            echo 'active';
//                        }
//                        ?><!--">-->
<!--                            <a class="nav-link" href="gents.php">GENTS</a>-->
<!--                        </li>-->
<!---->
<!--                        <li class="nav-item --><?php
//                        if ($page == 'ladies') {
//                            echo 'active';
//                        }
//                        ?><!--">-->
<!--                            <a class="nav-link" href="ladies.php">LADIES</a>-->
<!--                        </li>-->
<!---->
<!--                        <li class="nav-item --><?php
//                        if ($page == 'kids') {
//                            echo 'active';
//                        }
//                        ?><!--">-->
<!--                            <a class="nav-link" href="kids.php">KIDS</a>-->
<!--                        </li>-->

<!--                        <li class="nav-item --><?php
//                        if ($page == 'contact') {
//                            echo 'active';
//                        }
//                        ?><!--">-->
<!--                            <a class="nav-link" href="#">CONTACT</a>-->
<!--                        </li>-->
                    </ul>
                </div>
            </div>
        </nav>
        <!------------------------------------end nav------------------------------------------------------>