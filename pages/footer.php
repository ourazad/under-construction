<div class="footer_full">
    <div class="container">
        <div class="row copyright">
            <div class="col-md-6 left">
                <p>Copyright ©  2018</p>
            </div>
            <div class="col-md-6 right">
                <p>Developed by <a href="http://www.azoncode.com/" target="_blank">Azoncode</a></p>
            </div>
        </div>
    </div>
</div>
<!------------------------------------end footer------------------------------------------------------>
<!--<div class="copyright_full">
    <div class="container copyright">
        <div class="row">
            <div class="col-md-6 left">
                <p>2018 © Helvetius. ALL Rights Reserved. </p>
            </div>
            <div class="col-md-6 right">
                <p>Developed by <a href="https://www.azoncode.com/" target="_blank">Azoncode</a></p>
            </div>
        </div>
    </div>
</div>-->
<!------------------------------------end copyright------------------------------------------------------>
<a class="btn btn-outline-primary" id="topBtn" onclick="topFunction();"><i class="fas fa-angle-up"></i></a>
<!--<button id="topBtn" onclick="topFunction();"><i class="fas fa-angle-double-up" style="font-size: 48px; color: red;"></i></button>-->
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="includes/js/jquery-3.3.1.slim.min.js" ></script>
<script src="includes/js/popper.min.js" ></script>
<script src="includes/js/bootstrap.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
<script>
    baguetteBox.run('.tz-gallery');
</script>
<script>
    $(document).ready(function () {
        $('.product-slider').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1000,
            arrows: false,
            dots: false,
            pauseOnHover: false,
            responsive: [{
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3
                    }
                }, {
                    breakpoint: 520,
                    settings: {
                        slidesToShow: 3
                    }
                }]
        });
    });
</script>

<script type="text/javascript">
    function myfunc() {
        window.print();
    }
</script>
<script type="text/javascript">
    window.onscroll = function () {
        scrollFunction();
    };
    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            document.getElementById("topBtn").style.display = "block";
//            document.getElementById("topBtn").style.opacity = "0.8";
        } else {
            document.getElementById("topBtn").style.display = "none";
        }
    }
    function topFunction() {
        document.body.scrollTop = 0; // for safari
        document.documentElement.scrollTop = 0; // chromr , firefox , IE and Opera
    }
</script>

</body>
</html>